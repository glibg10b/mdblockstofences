#ifndef FOREACHLINE_HPP
#define FOREACHLINE_HPP

#include "RuntimeError.hpp"
#include "errorprint.hpp"

#include <functional>
#include <istream>
#include <string_view>
#include <string>

inline void forEachLine(std::istream& is, std::string_view path,
		std::function<void(std::string_view)> pred)
{
	std::string line{};
	while (true)
	{
		std::getline(is, line);
		if (is.eof()) return;

		if (is.bad())
			throw RuntimeError{ ExitCode::badFileRead,
				std::string{path} };

		if (is.fail())
		{
			printErr("Unknown failure reading from ", path,
					". Continuing...");
			is.clear();
			continue;
		}

		pred(line);
	}
}

#endif // !FOREACHLINE_HPP
