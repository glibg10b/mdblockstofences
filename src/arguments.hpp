#ifndef ARGUMENTS_HPP
#define ARGUMENTS_HPP

#include "RuntimeError.hpp"
#include "ExitCode.hpp"
#include "mdcodeconversion.hpp"

#include <iostream>
#include <string_view>

struct Args {
	bool help{ false };
	std::string_view language{};
};

inline void processLongOption(std::string_view option, Args& arg) {
	if (option == "help") arg.help = true;
}

inline std::string usage() {
	return "usage:\t" + std::string{g_programName.get()} + " [language]\n"
		"\t" + std::string{g_programName.get()} + " <--help>";
}

inline Args getArgs(int argc, char* argv[]) {
	Args ret{};

	for (int i{ 1 }; i < argc; ++i)
	{
		std::string_view arg{ argv[i] };
		if (arg.starts_with("--"))
			processLongOption(arg.substr(2), ret);
		else if (ret.language.empty()) ret.language = arg;
		else throw RuntimeError{ ExitCode::badArg, "language may only "
			"be specified once.\n" + usage() };
	}

	return ret;
}

inline std::string_view getLanguage(const Args& args) {
	if (args.language.empty()) return "text";
	else return args.language;
}

inline ExitCode::Value processArgs(const Args& args) {
	if (args.help) std::cout << usage() << '\n';
	else convertCodeBlocks(std::cin, "stdin", getLanguage(args));

	return ExitCode::success;
}

#endif // !ARGUMENTS_HPP
