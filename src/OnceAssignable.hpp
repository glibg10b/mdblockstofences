#ifndef ONCEASSIGNABLE_HPP
#define ONCEASSIGNABLE_HPP

#include <mutex>

template <typename T>
class OnceAssignable {
public:
	OnceAssignable() = default;
	OnceAssignable(const OnceAssignable&) = delete;
	OnceAssignable& operator=(const OnceAssignable&) = delete;

	const auto& operator=(T&& x) {
		std::call_once(m_onceFlag, [&]{ m_data = x; });
		return *this;
	}
	const T& get() const { return m_data; }
	operator const T&() const { return m_data; }
private:
	T m_data{};
	std::once_flag m_onceFlag{};
};

#endif // !ONCEASSIGNABLE_HPP
