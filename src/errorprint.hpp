#ifndef ERRORPRINT_HPP
#define ERRORPRINT_HPP

#include "Package.hpp"
#include "programname.hpp"

#include <iostream>

using namespace std::literals;

template <typename... Insertable>
void printErr(const Insertable&... msg)
{
	((std::cerr << g_programName.get() << ": ") << ... << msg) << '\n';
}

inline void printUnknownErr() {
	printErr("Unknown error. Please report this to " PACKAGE_BUGREPORT
			"\n");
}

template <typename... Insertable>
void printUnknownErr(const Insertable&... msg) {
	printErr("Unknown error ("s, msg..., "). Please report this to "
			PACKAGE_BUGREPORT "\n");
}

#endif // !ERRORPRINT_HPP
