#ifndef PROGRAMNAME_HPP
#define PROGRAMNAME_HPP

#include "OnceAssignable.hpp"

#include <string_view>

extern OnceAssignable<std::string_view> g_programName;

#endif // !PROGRAMNAME_HPP
