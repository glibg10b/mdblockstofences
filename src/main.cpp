#include "ExitCode.hpp"
#include "RuntimeError.hpp"
#include "arguments.hpp"
#include "errorprint.hpp"

#include <stdexcept>

int main(int argc, char* argv[]) try {
	g_programName = argv[0];

	return processArgs(getArgs(argc, argv));
}
catch (RuntimeError e) {
	printErr(e.what());
	return e.exitCode();
}
catch (std::exception e) {
	printUnknownErr(e.what());
	return ExitCode::unknown;
}
catch (...) {
	printUnknownErr();
	throw;
}
