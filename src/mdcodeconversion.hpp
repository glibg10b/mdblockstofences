#ifndef MDCODECONVERSION_HPP
#define MDCODECONVERSION_HPP

#include "RuntimeError.hpp"

#include "foreachline.hpp"

#include <iostream>
#include <string_view>
#include <string>
#include <utility>

inline void convertLine(std::string_view line, std::string_view language,
		bool& inCodeBlock, bool& prevLineIsEmpty)
{
	if (prevLineIsEmpty && line.empty()) std::cout << '\n';

	if (!line.empty())
	{
		if (std::exchange(inCodeBlock, line.starts_with("    ")))
		{
			if (!inCodeBlock) std::cout << "```\n";
			if (prevLineIsEmpty) std::cout << '\n';
		}
		else
		{
			if (prevLineIsEmpty) std::cout << '\n';
			if (inCodeBlock) std::cout << "```" << language << '\n';
		}

		if (inCodeBlock) std::cout << line.substr(4) << '\n';
		else             std::cout << line << '\n';
	}

	prevLineIsEmpty = line.empty();
}

inline void convertCodeBlocks(std::istream& is, std::string_view path,
		std::string_view language)
{
	if (is.fail())
		throw RuntimeError{ ExitCode::badFileOpenRead, std::string{path}
		};

	bool inCodeBlock{ false };
	bool prevLineIsEmpty{ false };

	forEachLine(is, path, [&](std::string_view line) { convertLine(line,
				language, inCodeBlock, prevLineIsEmpty); });
}

#endif // !MDCODECONVERSION_HPP
