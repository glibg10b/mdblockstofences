# mdblockstofences

Converts indented markdown code blocks to code fences

## Demonstration

input.md:

```markdown
# This is a test file

    #include <iostream>
    
    int main() {
    	std::cout << "Hello, world!\n";
    }
```

```sh
cat input.md | mdblockstofences c++ > output.md
```

output.md:

````markdown
# This is a test file

```c++
#include <iostream>

int main() {
	std::cout << "Hello, world!\n";
}
```
````

## Usage

```text
usage:	bin/mdblockstofences [language]
	bin/mdblockstofences <--help>
```

## Building and installing

```sh
# 1. Extract the sources
tar xf mdblockstofences-0.1.0.tar.xz

# 2. Build the executable
cd mdblockstofences-0.1.0
./configure
make -j$(nproc)

# 3. Run without installing (optional)
bin/mdblockstofences

# 4. Install and run the executable (installing requires root)
make install
hash -r
mdblockstofences
```
